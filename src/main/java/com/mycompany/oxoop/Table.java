/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.oxoop;

/**
 *
 * @author informatics
 */
public class Table {

    private String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    private Player player1;
    private Player player2;
    private Player CP;
    private int turnCount = 0;
    private int row, col;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.CP = player1;
    }

    public String[][] getTable() {
        return table;
    }

    public Player getCP() {
        return CP;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == "-") {
            table[row - 1][col - 1] = CP.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        turnCount++;
        if (CP == player1) {
            CP = player2;
        } else {
            CP = player1;
        }
    }

    public boolean checkWin() {
        if (checkRow()) {
            saveWin();
            return true;
        }
        return false;
    }

    public boolean checkRow() {
        return table[row - 1][0] != "-" && table[row - 1][0] == table[row - 1][1] && table[row - 1][2] == table[row - 1][1];
    }

    public boolean checkDraw() {
        if (turnCount == 9) {
            saveDraw();
            return true;
        }
        return false;
    }

    private void saveWin() {
        if (player1 == getCP()) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.win();
        }
    }
    private void saveDraw(){
        player1.draw();
        player2.draw();
    }
}
